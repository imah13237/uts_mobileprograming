import React, {useState, useEffect} from 'react';
import {View, Text, TouchableOpacity, TextInput} from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';

const App = () => {
  const [email, setEmail] = useState('');

  return (
    <View style={{ backgroundColor: '#D8BFD8', flex: 1}}>
      <View
        style={{
          flexDirection: 'row',
          marginTop: 20,
          marginHorizontal: 20,
          borderWidth: 2,
          padding: 10,
          borderRadius: 8,
          borderColor: email.length > 0 ? '#3453f6' : '#9fa6c3',
          backgroundColor: '#FFFFFF',
        }}>
        <TextInput
          value={email}
          onChangeText={text => setEmail(text)}
          style={{backgroundColor: '#696969', flex: 1}}
          placeholder="Masukkan Email"
        />
        <TouchableOpacity
            
          style={{
            backgroundColor: email.length > 0 ? '#3453f6' : '#9fa6c3',
            justifyContent: 'center',
            alignItems: 'center',
            paddingHorizontal: 40,
            borderRadius: 8,
            marginLeft: 20,
          }}>
          <Text style={{color: '#FFFFFF', fontWeight: 'bold'}}>Daftar</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default App;