import React, {Component} from 'react';
import {View, Text,TouchableOpacity} from 'react-native';

class Home extends Component {
  constructor(props){
    super(props);
    this.state ={};
    data =[]
  }

  getdata = () =>{
    fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response => response.json())
    .then(json => {
      this.setState({data:json});
      console.log(json);
    })
    .catch(error => {
      console.error(error);
    });

  }

  render(){
    return <View style={{flex :1,justifyContent:"center",alignItems:"center", backgroundColor: "#D8BFD8"}}>
<TouchableOpacity onPress={() => this.getdata()}>
  <Text style={{color: "black", fontWeight: "bold", fontSize: 20}}>Selamat Datang Di Warung Makan Kami</Text>
</TouchableOpacity>
    </View>;
  }
}
  

export default Home