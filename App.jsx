import React, { useState } from 'react';
import { View, Text, TouchableOpacity, StatusBar } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Home from './src/components/Home';
import Menu from './src/components/Menu';
import Bayar from './src/components/Bayar';
import Profile from './src/components/Profile';


const App = () => {
  const [activeMenu, setActiveMenu] = useState('Home');
  const Tab = createBottomTabNavigator();

  return (
    <View style={{ flex: 1, backgroundColor: '#FAFAFA' }}>
      {activeMenu == 'Home' && <Home />}
      {activeMenu == 'Menu' && <Menu />}
      {activeMenu == 'Bayar' && <Bayar />}
      {activeMenu == 'Profile' && <Profile />}
      
      <View
        style={{
          backgroundColor: '#FFFFFF',
          flexDirection: 'row',
          paddingVertical: 6,
          borderRadius: 10,
          borderTopColor: '#bdbdbd',
          justifyContent: 'space-around', 
          alignItems: 'center', 
          position: 'absolute', 
          bottom: 0, 
          left: 0, 
          right: 0, 
        }}>
        <TouchableOpacity
          onPress={() => setActiveMenu('Home')}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: activeMenu == 'Home' ? '#B0C4DE' : '#FFFFFF',
            elevation: activeMenu == 'Home' ? 2 : 0,
            paddingVertical: 9,
            borderRadius: 12,
          }}>
          <Icon name="home" size={22} color="#5e5ce5" />
          <Text style={{ color: '#5e5ce5' }}>Home</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setActiveMenu('Menu')}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            paddingVertical:9,
            borderRadius: 12,
            backgroundColor: activeMenu == 'Menu' ? '#B0C4DE' : '#FFFFFF',
            elevation: activeMenu == 'Menu' ? 2 : 0,
          }}>
          <Icon name="boxes" size={22} color="#6e6b72" />
          <Text style={{ color: activeMenu == 'Menu' ? '#FFFFFF' : '#9ea3b0' }}>
            Menu
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setActiveMenu('Bayar')}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: activeMenu == 'Bayar' ? '#B0C4DE' : '#FFFFFF',
            elevation: activeMenu == 'Bayar' ? 2 : 0,
            paddingVertical: 9,
            borderRadius: 12,
          }}>
          <Icon name="credit-card" size={22} color="#6e6b72" />
          <Text style={{ color: activeMenu == 'Bayar' ? '#FFFFFF' : '#9ea3b0' }}>
            Bayar
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setActiveMenu('Profile')}
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: activeMenu == 'Profile' ? '#B0C4DE' : '#FFFFFF',
            elevation: activeMenu == 'Profile' ? 2 : 0,
            paddingVertical: 9,
            borderRadius: 12,
          }}>
          <Icon name="user" size={22} color="#6e6b72" />
          <Text style={{ color: activeMenu == 'Profile' ? '#FFFFFF' : '#9ea3b0' }}>
            Profile
          </Text>
        </TouchableOpacity>
      </View>
      <StatusBar backgroundColor={'#FAFAFA'} barStyle="dark-content" />
    </View>
  );
};

export default App;
